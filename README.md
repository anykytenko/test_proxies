# README #

The first project using Python, Django and PostgreSQL
It has 2 main parts:
1. You should visit start page (for example, "http://127.0.0.1:8000/") and press the "Import CSV" button.
The application will import the static CSV file (as for the task). 
After that you will be redirected to a page which provide you with controls to make requests and filter given table;

2. You can use controls at the top of the page to filter data by County, Channel and Subnet clicking "Request" button for this purpose.
You can also make a request just inputting url to your browser address field like this "<host_name>:<port>/mapping_table/<filter_by>/<filter_value>/", where 
    - <host_name> is the host name of your application;
    - <port> is the port of your application;
    - <filter_by> is the identifier {0: Country, 1: Channel, 2: Subnet};
    - <filter_value> is the identifier of an entity you have selected using <filter_by> parameter.
For example, "http://127.0.0.1:8000/mapping_table/2/10/"

Right of these controls you can see label "Request time:" with number of millis (ms) that says you how long the request works.


### What is this repository for? ###

This repo is for review

### How do I get set up? ###

* Install Python 2.7
* Install Django 1.11
* Set up in "\site\mysite\mysite\settings.py": 
    - "HOST" and "PORT" of your application; 
    - "DB_USER", "DB_PASSWORD", "DB_HOST", "DB_PORT", "DB_NAME" of your database.
* Dependencies:
    - django;
    - os;
    - sys;
    - sqlalchemy;
    - pandas;
    - netaddr;
    - socket;
    - struct;
    - time;
* Deployment instructions:
    - cd \site\mysite
    - python manage.py runserver

### Who do I talk to? ###

* Reviewer