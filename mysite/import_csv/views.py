from django.http import HttpResponse
from controller.import_csv_to_db import execute
from django.template import loader
from django.conf import settings

def import_csv(request):
    execute()
    template = loader.get_template("import_csv.html")
    dict = {"HOST": settings.HOST, "PORT": settings.PORT}
    return HttpResponse(template.render(dict))
