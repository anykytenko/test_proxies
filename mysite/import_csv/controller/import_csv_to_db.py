from Ip import Ip
from sqlalchemy import create_engine
import pandas as pd
import netaddr
from django.conf import settings


def build_csv_df():
    return pd.read_csv(settings.HOST + ":" + settings.PORT +"/static/csv/test_proxies.csv", "\t", names=["country_iso_code", "ip", "channel"], skipinitialspace=True)


def build_countries_df(data_frame):
    result = pd.DataFrame(data_frame["country_iso_code"].drop_duplicates())
    result.reset_index()
    result["id"] = result.index
    return result


def build_channels_df(data_frame):
    result = pd.DataFrame(data_frame["channel"].drop_duplicates())
    result.reset_index()
    result["id"] = result.index
    return result


def build_subnets_df(data_frame):
    subnets = {}
    for index, row in data_frame.iterrows():
        ip = Ip(row["ip"])
        if ip.version == 4:
            subnet = netaddr.iprange_to_cidrs(ip.value[0] + "." + ip.value[1] + "." + ip.value[2] + ".0",
                                              ip.value[0] + "." + ip.value[1] + "." + ip.value[2] + ".255")[0]
        else:
            subnet = netaddr.iprange_to_cidrs(ip.value[0] + ":" + ip.value[1] + "::",
                                              ip.value[0] + ":" + ip.value[1] + ":ffff:ffff:ffff:ffff:ffff:ffff")[0]
        subnets.update({subnet: index})
    result_dict = {"subnet": subnets.keys()}
    result = pd.DataFrame(result_dict)
    result.reset_index()
    result["id"] = result.index
    return result


def build_mapping_df(csv_df, countries_df, channels_df, subnets_df):
    result = {}
    result_df = csv_df
    countries_dict = {}
    channels_dict = {}
    subnets_dict = {}
    for index, row in countries_df.iterrows():
        countries_dict.update({row["country_iso_code"]: index})
    for index, row in channels_df.iterrows():
        channels_dict.update({row["channel"]: index})
    for index, row in subnets_df.iterrows():
        subnets_dict.update({row["subnet"]: index})
    subnets = []
    for index, row in result_df.iterrows():
        ip = Ip(row["ip"])
        if ip.version == 4:
            subnet = netaddr.iprange_to_cidrs(ip.value[0] + "." + ip.value[1] + "." + ip.value[2] + ".0",
                                              ip.value[0] + "." + ip.value[1] + "." + ip.value[2] + ".255")[0]
        else:
            subnet = netaddr.iprange_to_cidrs(ip.value[0] + ":" + ip.value[1] + "::",
                                              ip.value[0] + ":" + ip.value[1] + ":ffff:ffff:ffff:ffff:ffff:ffff")[0]
        subnets.append(subnet)
    result_df["subnet"] = subnets
    result_df["country_iso_code"] = result_df["country_iso_code"].map(countries_dict)
    result_df["channel"] = result_df["channel"].map(channels_dict)
    result_df["subnet"] = result_df["subnet"].map(subnets_dict)
    result_df.reset_index()
    result_df["id"] = result_df.index
    result.update({"mapping_df": result_df})
    result.update({"countries_dict": countries_dict})
    result.update({"channels_dict": channels_dict})
    result.update({"subnets_dict": subnets_dict})
    return result


def execute():
    result = {}
    csv_df = build_csv_df()
    countries_df = build_countries_df(csv_df)
    channels_df = build_channels_df(csv_df)
    subnets_df = build_subnets_df(csv_df)
    mapping_result = build_mapping_df(csv_df, countries_df, channels_df, subnets_df)
    mapping_df = mapping_result.get("mapping_df")
    result.update({"countries": pd.DataFrame(countries_df["country_iso_code"]).to_dict().values()})
    result.update({"channels": channels_df.to_dict("index").values()})
    result.update({"subnets": subnets_df.to_dict("index").values()})

    engine = create_engine('postgresql://' + settings.DB_USER + ':' + settings.DB_PASSWORD + '@' + settings.DB_HOST +
                           ':' + settings.DB_PORT + '/' + settings.DB_NAME, echo=False)

    countries_df.to_sql(name='countries', con=engine, if_exists='replace', index=False)
    channels_df.to_sql(name='channels', con=engine, if_exists='replace', index=False)
    mapping_df.to_sql(name='mapping', con=engine, if_exists='replace', index=False)
    sql = "DROP TABLE IF EXISTS subnets; CREATE TABLE subnets (id SERIAL PRIMARY KEY, address INET); INSERT INTO subnets (id, address) VALUES "
    for key, value in mapping_result['subnets_dict'].items():
        sql += "(" + str(value) + ", '" + str(key) + "'),"
    sql = sql[0:-1] + ";"
    engine.connect().execute(sql)

    sql = "DROP TABLE IF EXISTS mapping; CREATE TABLE mapping " + \
          "(id SERIAL PRIMARY KEY, country_id INT, ip INET, subnet_id INT, channel_id INT); " + \
          "INSERT INTO mapping (id, country_id, ip, subnet_id, channel_id) VALUES "
    for index, row in mapping_df.iterrows():
        sql += "(" + str(index) + ", " + str(row[0]) + ", '" + str(row[1]) + "', " + str(row[3]) + ", " + str(
            row[2]) + "),"
    sql = sql[0:-1] + ";"
    engine.connect().execute(sql)

    return mapping_result
