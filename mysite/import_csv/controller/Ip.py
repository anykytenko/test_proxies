import IpUtils


class Ip:

    def __init__(self, string_ip):
        self.version = IpUtils.get_version(string_ip)
        if self.version == 6:
            self.value = IpUtils.normalize_ipv6(string_ip)
        else:
            if self.version == 4:
                self.value = IpUtils.normalize_ipv4(string_ip)
            else:
                self.value = "Is not IP"

