import socket
import struct


def split_for_groups(ipv6):
    result = ipv6.split(":")
    size = len(result)
    if size < 8:
        empty_index = -1
        i = 0
        while i < size:
            part = result[i]
            if part == "":
                empty_index = i
                break
            i += 1
        j = 0
        while j < 8 - size:
            result.insert(empty_index + j, "")
            j += 1
    return result


def normalize_group(group):
    result = group
    size = len(group)
    if size < 4:
        i = size
        while i < 4:
            result = "0" + result
            i += 1
    return result


def normalize_ipv6(ipv6):
    result = split_for_groups(ipv6)
    i = 0
    while i < 8:
        result[i] = normalize_group(result[i])
        i += 1
    return result


def normalize_ipv4(ipv4):
    return ipv4.split(".")


def get_version(ip):
    array = ip.split(".")
    if len(array) == 4:
        return 4
    array = ip.split(":")
    if len(array) <= 8:
        return 6
    return -1


def ip_to_long(ip):
    packed_ip = socket.inet_aton(ip)
    return struct.unpack("!L", packed_ip)[0]


def long_to_ip(long):
    return socket.inet_ntoa(struct.pack('!L', 2130706433))
