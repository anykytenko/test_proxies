from django.conf.urls import url

from . import views

urlpatterns = [
	url('', views.import_csv, name='import_csv'),
]
