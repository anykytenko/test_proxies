from django.conf.urls import include, url

urlpatterns = [
    url('import_csv/', include('import_csv.urls')),
    url('mapping_table/', include('mapping_table.urls')),
    url('', include('start.urls'))
]
