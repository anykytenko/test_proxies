from django.http import HttpResponse
from django.template import loader
from controller.filter_utils import filter_result
from django.conf import settings
import time


def mapping_table(request, filter_by, filter_value):
    start = int(round(time.time() * 1000))
    template = loader.get_template("mapping_table.html")
    result = filter_result(filter_by, filter_value)
    result.update({"filter_by": str(filter_by)})
    result.update({"filter_value": int(filter_value)})
    result.update({"HOST": settings.HOST, "PORT": settings.PORT})
    result.update({"time": int(round(time.time() * 1000)) - start})
    return HttpResponse(template.render(result))
