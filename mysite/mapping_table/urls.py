from django.conf.urls import url

from . import view

urlpatterns = [
	url(r'(\d+)/(\d+)', view.mapping_table, name='mapping_table')
]
