from django.http import HttpResponse
from django.template import loader


def start(request):
    template = loader.get_template("start.html")
    return HttpResponse(template.render())
