from django.conf.urls import url

from . import view

urlpatterns = [
	url('', view.start, name='start')
]
